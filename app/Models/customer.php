<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'company_name',
        'type',
        'work_number',
        'whatsapp_number',
        'address',
        'credit_limit',
        'is_active'
    ];


    protected $table = 'customers';
}
