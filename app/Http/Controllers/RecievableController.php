<?php

namespace App\Http\Controllers;

use App\Models\recievable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Support\Facades\DB; 

class RecievableController extends Controller
{
    // all recieve ables over all
    public function recievables()
    {
        $recievables = recievable::all();
        return response()->json(['recievables'=>$recievables]);
    }

    // recievables by customer id 
    public function recievableByCustomerId(Request $request)
    {
        $recievables = DB::table("recievables")->where("customer_id", $request->customer_id)
        ->orderBy("created_at", "desc")->get();
        return response()->json(['recievables'=>$recievables]);
    }

    // recievables by date
    public function recievableByDate(Request $request)
    {
        $recievables = DB::table("recievables")->where("created_at", $request->created_at)->get();
        return response()->json(['recievables'=>$recievables]);
    }

    // create new recievable
    public function createRecievable(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'type' => 'required|string|between:2,100',
            'customer_id' => 'required|integer', 
        ]);
        if ($validator->fails()) {
            return response()->json([$validator->errors()], 422);
        }
        
        
        $recievable = recievable::create(array_merge($validator->validated(), [
            "description"=>$request->description,
            "debit"=>($request->debit) ? $request->debit : 0,
            "credit"=>($request->credit) ? $request->credit : 0,
            "balance"=>($request->balance) ? $request->balance : 0,
            "status"=>($request->status) ? $request->status : 1,
        ]));
        return response()->json(['message' => 'recievable created successfully', 'recievable'=>$recievable]); 
    }

    // update recievable
    public function updateRecievable(Request $request)
    {

        $recievabl = recievable::findOrFail($request->id);

        $validator = Validator::make($request->all(),[
            'type' => 'required|string|between:2,100',
            'customer_id' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return response()->json([$validator->errors()], 422);
        }
        
        $recievabl->type = $request->type;
        $recievabl->customer_id = $request->customer_id;
        $recievabl->description = $request->description;
        $recievabl->debit = ($request->debit) ? $request->debit : 0;
        $recievabl->credit = ($request->credit) ? $request->credit : 0;
        $recievabl->balance = ($request->balance) ? $request->balance : 0;
        $recievabl->status = ($request->status) ? $request->status : 1; 
        
        if($recievabl->save())
        {
            return response()->json(['message' => 'recievable updated successfully', 'recievable'=>$recievabl]);
        }else{
            return response()->json(['error'=>"something went wrong!"]);
        }
    }

    // debit recievable
    public function debitRecievable(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'type' => 'required|string|between:2,100',
            'customer_id' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return response()->json([$validator->errors()], 422);
        }

        if(recievable::where("customer_id", $request->customer_id)->first() != null){
           $rec = DB::table("recievables")->where("customer_id", $request->customer_id)->latest()->first();

            $newrec = new recievable;

            $newrec->type = $request->type;
            $newrec->customer_id = $request->customer_id;
            $newrec->description = $request->description;
            $newrec->debit = $request->debit; 
            $newrec->balance = $rec->balance + $request->debit;
            $newrec->status = $request->status; 

            if($newrec->save())
            {
                return response()->json(['message' => 'recievable debited successfully', 'recievable'=>$newrec]);
            }else{
                return response()->json(['error'=>"something went wrong!"]);
            }
            
        }else{
            return $this->createRecievable($request);
        }
    }

    // credit recievable
    public function creditRecievable(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'type' => 'required|string|between:2,100',
            'customer_id' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return response()->json([$validator->errors()], 422);
        }

        if(recievable::where("customer_id", $request->customer_id)->first() != null){
           $rec = DB::table("recievables")->where("customer_id", $request->customer_id)->latest()->first(); 

            $newrec = new recievable;

            $newrec->type = $request->type;
            $newrec->customer_id = $request->customer_id;
            $newrec->description = $request->description; 
            $newrec->credit = $request->credit;
            $newrec->balance = $rec->balance - $request->credit;
            $newrec->status = $request->status; 

            if($newrec->save())
            {
                return response()->json(['message' => 'recievable credited successfully', 'recievable'=>$newrec]);
            }else{
                return response()->json(['error'=>"something went wrong!"]);
            }
            
        }else{ 
            return $this->createRecievable($request);
        }
    }

    // delete recievable
    public function deleteRecievable(Request $request)
    {
        $recievabl = recievable::findOrFail($request->id);

        if($recievabl->delete())
        {
            return response()->json(['message' => 'recievable deleted successfully']);
        }else{
            return response()->json(['error'=>"something went wrong!"]);
        }
    }
}
