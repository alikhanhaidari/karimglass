<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator; 
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class ProductsController extends Controller
{
    public function addProduct(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|between:2,100',
            'type' => 'required|string',
            'tax_type' => 'required|string',
            'sku' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return response()->json([$validator->errors()], 422);
        }
        
        $user = Auth::guard()->user();
        
        $product = Products::create(array_merge($validator->validated(),
                 ['user_id' => $user->id]));
        return response()->json(['message' => 'product created successfully', 'product'=>$product]);
    }


    public function updateProduct(Request $request)
    {
        $product = Products::findOrFail($request->id);

        $validator = Validator::make($request->all(),[
            'name' => 'required|string|between:2,100',
            'type' => 'required|string',
            'tax_type' => 'required|string',
            'sku' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return response()->json([$validator->errors()], 422);
        }
        
        $user = Auth::guard()->user();

        $product->name = $request->name;
        $product->type = $request->type;
        $product->unit_id = $request->unit_id;
        $product->brand_id = $request->brand_id;
        $product->category_id = $request->category_id;
        $product->tax = $request->tax;
        $product->tax_type = $request->tax_type;
        $product->enable_stock = ($request->enable_stock) ? $request->enable_stock : 0;
        $product->alert_quantity = $request->alert_quantity;
        $product->sku = $request->sku;
        $product->barcode_type = $request->barcode_type;
        $product->enable_serial_no = ($request->enable_serial_no ? $request->enable_serial_no : 0);
        $product->weight = $request->weight;
        $product->user_id = $user->id;

        if ($product->save()) {
            return response()->json(['message' => 'product updated successfully', 'product'=>$product]);
        } else {
            return response()->json(['message' => 'something went wrong'], 401);
        }
    }

    public function allProducts()
    {
        $products = Products::all();
        // foreach ($products as $product) {
        //     $product = array_merge(['user' => $product->user]);
        // }
        return response()->json(['products'=>$products]);
    }

    public function allProductsWithUserInfo()
    {
        $products = Products::all();
        foreach ($products as $product) {
            $product = array_merge(['user' => $product->user]);
        }
        return response()->json(['products'=>$products]);
    }

    public function allProductsWithFilter(Request $request)
    {
        $products = Products::where([$request->column_name => $request->column_value])->get();
        
        return response()->json(['products'=>$products]);
    }

    public function deleteProduct(Request $request)
    {
        $product = Products::findOrFail($request->id);
        
        if ($product->delete()) {
            return response()->json(['message' => 'product deleted successfully']);
        } else {
            return response()->json(['message' => 'something went wrong'], 401);
        }
    }

}
