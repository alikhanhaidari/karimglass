<?php

namespace App\Http\Controllers;

use App\Models\customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 

class CustomerController extends Controller
{
    public function customers()
    {
        $customers = customer::all();

        return response()->json(['customers'=>$customers]);
    }

    public function customerById(Request $request)
    {
        $customer = customer::findOrFail($request->id);

        return response()->json(['customer'=>$customer]);
    }

    public function createCustomer(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'name' => 'required|string|between:2,100',
            'company_name' => 'required|string',
            'type' => 'required|string', 
        ]);
        if ($validator->fails()) {
            return response()->json([$validator->errors()], 422);
        }
        
        
        $customer = customer::create($validator->validated());
        return response()->json(['message' => 'customer created successfully', 'customer'=>$customer]); 
    }

    public function updateCustomer(Request $request)
    {

        $custmr = customer::findOrFail($request->id);

        $validator = Validator::make($request->all(),[
            'name' => 'required|string|between:2,100',
            'company_name' => 'required|string',
            'type' => 'required|string',  
        ]);
        if ($validator->fails()) {
            return response()->json([$validator->errors()], 422);
        }
        
        $custmr->name = $request->name;
        $custmr->company_name = $request->company_name;
        $custmr->type = $request->type;
        $custmr->work_number = $request->work_number;
        $custmr->whatsapp_number = $request->whatsapp_number;
        $custmr->address = $request->address;
        $custmr->credit_limit = $request->credit_limit;
        $custmr->is_active = $request->is_active;
        
        if($custmr->save())
        {
            return response()->json(['message' => 'customer updated successfully', 'customer'=>$custmr]);
        }else{
            return response()->json(['error'=>"something went wrong!"]);
        }
    }

    public function deleteCustomer(Request $request)
    {
        $custmr = customer::findOrFail($request->id);

        if($custmr->delete())
        {
            return response()->json(['message' => 'customer deleted successfully']);
        }else{
            return response()->json(['error'=>"something went wrong!"]);
        }
    }
}
