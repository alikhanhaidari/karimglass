<?php

namespace App\Http\Controllers;

use App\Models\vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 

class VendorController extends Controller
{
    public function vendors()
    {
        $vendors = vendor::all();

        return response()->json(['vendors'=>$vendors]);
    }

    public function vendorById(Request $request)
    {
        $vendor = vendor::findOrFail($request->id);

        return response()->json(['vendor'=>$vendor]);
    }

    public function addVendor(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'name' => 'required|string|between:2,100',
            'company_name' => 'required|string',
            'type' => 'required|string', 
        ]);
        if ($validator->fails()) {
            return response()->json([$validator->errors()], 422);
        }
        
        
        $vendor = vendor::create($validator->validated());
        return response()->json(['message' => 'vendor created successfully', 'vendor'=>$vendor]);

        $vendor = vendor::findOrFail($request->id);

        return response()->json(['vendor'=>$vendor]);
    }

    public function updateVendor(Request $request)
    {

        $vendr = vendor::findOrFail($request->id);

        $validator = Validator::make($request->all(),[
            'name' => 'required|string|between:2,100',
            'company_name' => 'required|string',
            'type' => 'required|string',  
        ]);
        if ($validator->fails()) {
            return response()->json([$validator->errors()], 422);
        }
        
        $vendr->name = $request->name;
        $vendr->company_name = $request->company_name;
        $vendr->type = $request->type;
        $vendr->work_number = $request->work_number;
        $vendr->whatsapp_number = $request->whatsapp_number;
        $vendr->address = $request->address;
        $vendr->credit_limit = $request->credit_limit;
        $vendr->is_active = $request->is_active;
        
        if($vendr->save())
        {
            return response()->json(['message' => 'vendor updated successfully', 'vendor'=>$vendr]);
        }else{
            return response()->json(['error'=>"something went wrong!"]);
        }
    }

    public function deleteVendor(Request $request)
    {
        $vendr = vendor::findOrFail($request->id);

        if($vendr->delete())
        {
            return response()->json(['message' => 'vendor deleted successfully']);
        }else{
            return response()->json(['error'=>"something went wrong!"]);
        }
    }
}
