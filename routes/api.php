<?php

use App\Http\Controllers;
use App\Http\Controllers\TodoController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\CustomerController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

*/



Route::group([
    'middleware' => 'api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'auth',
], function($router) {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('logout', 'AuthController@logout');
    Route::get('profile', 'AuthController@profile');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('open', ['TodoController@open']);    
});

Route::group([
    'middleware' => 'api',
    'namespace' => 'App\Http\Controllers',
], function($router) {
    Route::resource('todos', 'TodoController@login');

    // Products Routes
    Route::post('addProduct', 'ProductsController@addProduct');
    Route::post('updateProduct', 'ProductsController@updateProduct');
    Route::get('products', 'ProductsController@allProducts');
    Route::get('allProductsWithUserInfo', 'ProductsController@allProductsWithUserInfo');
    Route::post('allProductsWithFilter', 'ProductsController@allProductsWithFilter');
    Route::post('deleteProduct', 'ProductsController@deleteProduct');

    // Vendors Routes
    Route::get('vendors', 'VendorController@vendors');
    Route::post('vendorById', 'VendorController@vendorById');
    Route::post('addVendor', 'VendorController@addVendor');
    Route::post('updateVendor', 'VendorController@updateVendor');
    Route::post('deleteVendor', 'VendorController@deleteVendor');

    // Customers Routes
    Route::get('customers', 'CustomerController@customers');
    Route::post('customerById', 'CustomerController@customerById');
    Route::post('createCustomer', 'CustomerController@createCustomer');
    Route::post('updateCustomer', 'CustomerController@updateCustomer');
    Route::post('deleteCustomer', 'CustomerController@deleteCustomer');

    // Recievable Routes
    Route::get('recievables', 'RecievableController@recievables');
    Route::post('recievableByCustomerId', 'RecievableController@recievableByCustomerId');
    Route::post('recievableByDate', 'RecievableController@recievableByDate');
    Route::post('createRecievable', 'RecievableController@createRecievable');
    Route::post('updateRecievable', 'RecievableController@updateRecievable');
    Route::post('debitRecievable', 'RecievableController@debitRecievable');
    Route::post('creditRecievable', 'RecievableController@creditRecievable');
    Route::post('deleteRecievable', 'RecievableController@deleteRecievable');

    // payable Routes
    Route::get('payables', 'PayableController@payables');
    Route::post('payableByVendorId', 'PayableController@payableByVendorId');
    Route::post('payableByDate', 'PayableController@payableByDate');
    Route::post('createPayable', 'PayableController@createPayable');
    Route::post('updatePayable', 'PayableController@updatePayable');
    Route::post('debitPayable', 'PayableController@debitPayable');
    Route::post('creditPayable', 'PayableController@creditPayable');
    Route::post('deletePayable', 'PayableController@deletePayable');

});